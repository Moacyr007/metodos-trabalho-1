#include <string>
#include <iostream>
#include<string>

#include "exprtk.hpp"

using namespace std;

string funcao;
string derivada;

double f(double y) {
	using namespace std;

	typedef exprtk::symbol_table<double> symbol_table_t;
	typedef exprtk::expression<double>     expression_t;
	typedef exprtk::parser<double>             parser_t;

	std::string expression_string = funcao;

	symbol_table_t symbol_table;
	symbol_table.add_variable("x", y);
	symbol_table.add_constants();

	expression_t expression;
	expression.register_symbol_table(symbol_table);

	parser_t parser;
	parser.compile(expression_string, expression);

	y = expression.value();

	return y;

}


double derivf(double y) {
	using namespace std;

	typedef exprtk::symbol_table<double> symbol_table_t;
	typedef exprtk::expression<double>     expression_t;
	typedef exprtk::parser<double>             parser_t;

	std::string expression_string = derivada;

	symbol_table_t symbol_table;
	symbol_table.add_variable("x", y);
	symbol_table.add_constants();

	expression_t expression;
	expression.register_symbol_table(symbol_table);

	parser_t parser;
	parser.compile(expression_string, expression);

	y = expression.value();

	return y;

}


int main()
{
	using namespace std;

	double a, b, precisao, x = 0;
	int escolha;

	cout << "Escolha o metodo \n";
	cout << "1. Metodo da bissecao \n";
	cout << "2. Metodo da posicao falsa \n";
	cout << "3. Newton Raphson \n";
	cout << "4. Metodo da secante \n";

	cin >> escolha;

	if (escolha == 1)
	{

		cout << "____Metodo da bissecao____\n";
		cout << "Digite a funcao: \n";
		cin >> funcao;

		cout << "Digite a precisao: \n";
		cin >> precisao;

		cout << "Digite o chute 1: \n";
		cin >> a;

		cout << "Digite o chute 2: \n";
		cin >> b;
	
	
		
		if ((f(a)*f(b)) < 0)
		{
			
			do
			{
				x = ((a + b) / 2);

				if ((f(a)*f(x)) < 0)
				{
					b = x;
				//	cout << "b:" << b << "\n";
				}	
				else
				{
					a = x;
					//cout << "a:" << a << "\n";
				}
				cout << "x:" << x << "\n";
			} while ((abs(f(x)) > precisao));
		
			cout << "O 0 da funcao eh: " << x << " \n";
		}
		else
		{
			cout << "Nao ha 0 da funcao para os chutes dados \n";
		}
	}







	else if (escolha == 2)
	{
		cout << "___Metodo da posicao falsa___ \n";
		cout << "Digite a funcao: \n";
		cin >> funcao;

		cout << "Digite a precisao: \n";
		cin >> precisao;

		cout << "Digite o chute 1: \n";
		cin >> a;

		cout << "Digite o chute 2: \n";
		cin >> b;


		if ((f(a)*f(b)) < 0)
		{
			x = (a*f(b)-b*f(a))/(f(b)-f(a));

			while (f(x) > precisao)
			{
				if ((f(a)*f(b)) < 0)
				{
					a = x;
				}
				else
				{
					b = x;
				}
				x = (a*f(b) - b * f(a)) / (f(b) - f(a));
			}
			cout << "O 0 da funcao eh: " << x << " \n";
		}
		else
		{
			cout << "Nao ha 0 da funcao para os chutes dados \n";
		}
	}
	else if (escolha == 3)
	{
		cout << "___Newton Raphson___ \n";
	}
	else if (escolha == 4)
	{
		cout << "___Metodo da secante___ \n";
	}
	else
	{
		cout << "Selecione 1, 2, 3 ou 4. \n";
	}

	if (escolha == 3)
	{
		cout << "___Metodo Newton Raphson-___ \n";
		cout << "Digite a funcao: \n";
		cin >> funcao;

		cout << "Digite da derivada da funcao\n";
		cin >> derivada;

		cout << "Digite a precisao: \n";
		cin >> precisao;

		cout << "Digite o chute inicial: \n";
		cin >> x;

		double h = f(x) / derivf(x);
		while (abs(h) >= precisao)
		{
			h = f(x) / derivf(x);
 
			x = x - h;
		}

		cout << "O valor eh: " << x;
	}
	/*   ARRUMAR ESSA PARTE
	if (escolha == 4)
	{
		cout << "___Metodo da Secante-___ \n";
		cout << "Digite a funcao: \n";
		cin >> funcao;

		cout << "Digite da derivada da funcao\n";
		cin >> derivada;

		cout << "Digite a precisao: \n";
		cin >> precisao;

		cout << "Digite o chute inicial: \n";
		cin >> x;

		double h = f(x) / derivf(x);
		while (abs(h) >= precisao)
		{
			h = f(x) / derivf(x);

			x = x - h;
		}

		cout << "O valor eh: " << x;
	}
	_________________________________________________________________________________________________



	// C++ Program to find root of an
	// equations using secant method
	#include <bits/stdc++.h>
	using namespace std;
	// function takes value of x and returns f(x)
	float f(float x)
	{
	// we are taking equation as x^3+x-1
	float f = pow(x, 3) + x - 1;
	return f;
	}

	void secant(float x1, float x2, float E)
	{
	float n = 0, xm, x0, c;
	if (f(x1) * f(x2) < 0) {
	do {
	// calculate the intermediate value
	x0 = (x1 * f(x2) - x2 * f(x1)) / (f(x2) - f(x1));

	// check if x0 is root of equation or not
	c = f(x1) * f(x0);

	// update the value of interval
	x1 = x2;
	x2 = x0;

	// update number of iteration
	n++;

	// if x0 is the root of equation then break the loop
	if (c == 0)
	break;
	xm = (x1 * f(x2) - x2 * f(x1)) / (f(x2) - f(x1));
	} while (fabs(xm - x0) >= E); // repeat the loop
	// until the convergence

	cout << "Root of the given equation=" << x0 << endl;
	cout << "No. of iterations = " << n << endl;
	} else
	cout << "Can not find a root in the given inteval";
	}

	// Driver code
	int main()
	{
	// initializing the values
	float x1 = 0, x2 = 1, E = 0.0001;
	secant(x1, x2, E);
	return 0;
	}

	*/

	system("pause");
	return 0;
}
